const fs = require('fs');                                                                       //fs module called

const path = require('path');                                                                   //path module called

const filePathDefault = path.join(__dirname, 'boards.json');                                     //defining a default path, if path not sent from user

function problem1(testIDRecieved, callBack, filePath = filePathDefault) {
    setTimeout(() => {
        fs.readFile(filePath, 'utf-8', ((err, data) => {                                             //reads file, returns data or error
            if (err) {
                callBack(err);                                                                       //returns error to callback function 
            }
            else {
                data = JSON.parse(data);

                data = data.find((ele) => {
                    return ele.id === testIDRecieved;
                });                                                                                 //finds the first element that matched the ID passed
                callBack(null, data);                                                               //returns data to callback
            }
        }));
    }, 2 * 1000);
};

module.exports = problem1;                                                                      //exporting the function