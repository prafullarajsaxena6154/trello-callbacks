const fs = require('fs');                                                                            //calling fs module
const listsData = require('./callback2.cjs');                                                        //calling function from callback2.cjs file
const cardsData = require('./callback3.cjs');                                                        //calling function from callback3.cjs file   

function problem5(name) {                                                                            //function for solving problem 5
    setTimeout(() => {
        fs.readFile('../boards.json', 'utf-8', (err, data) => {                                           //reads file and returns data from boards.json file
            if (err) {
                console.error(err);
            }
            else {
                data = JSON.parse(data);

                let thanosId = data.find((ele) => {
                    return (ele.name === name);
                }).id;                                                                                                            //finds and returns the element with matching name, and stores its ID
                console.log("ID for thanos data : " + thanosId);

                listsData(thanosId, (err, data) => {                                                                              //function from callback2 used to search ID obtained from boards.json file
                    if (err) {
                        console.error(err);
                    }
                    else {
                        let thanosList = data;
                        console.log("List for Thanos ID :");
                        console.log(thanosList);
                        let mindandSpaceId = thanosList.filter((ele) => ele.name === 'Mind' || ele.name === 'Space');            //finds and returns the element with matching name Mind and Space, and stores its ID
                        console.log("IDs for mind and soul list : ");
                        console.log(mindandSpaceId);
                        mindandSpaceId.map((ele) => {
                            cardsData(ele.id, (err, data) => {                                           //function from callback3 used to search ID from cards.json file
                                if (err) {
                                    console.error(err);
                                }
                                else {
                                    console.log(ele.name + " Cards:");
                                    console.log(data);
                                }
                            });
                            return ele;
                        });
                    }
                })
            }
        })
    }, 2 * 1000);
}
module.exports = problem5;
