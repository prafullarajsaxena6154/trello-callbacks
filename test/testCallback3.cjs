const path = require('path');

const filePath = path.join(__dirname,'../cards.json');                        //storing a resolved file path to be sent to the function

const idSent = 'qwsa221';                                           //id to be searched against, if no ID passed or wrong ID passed, returns undefined

let prob3Call = require('../callback3.cjs');                        

prob3Call(idSent, (err, data) => {                                  //function calls from callback3.cjs file, and sends a callback for output
    if (err) {
        console.error(err);
    }
    else {
        console.log(data);
    }
}, filePath);