const path = require('path');
const filePath = path.join(__dirname,'../lists.json');            //storing a resolved file path to be sent to the function


const idSent = 'mcu453ed';                              //id to be searched against, if no ID passed or wrong ID passed, returns undefined

let prob2Call = require('../callback2.cjs');

prob2Call(idSent, (err, data) => {                      //function calls from callback2.cjs file, and sends a callback for output
    if (err) {
        console.error(err);
    }
    else {
        console.log(data);
    }
}, filePath);