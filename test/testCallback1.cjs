const path = require('path');
const filePath = path.join(__dirname,'../boards.json');               //storing a resolved file path to be sent to the function

const idSent = 'mcu453ed';                                  //id to be searched against, if no ID passed or wrong ID passed, returns undefined

let prob1Call = require('../callback1.cjs');

prob1Call(idSent, (err, data) => {                          //function calls from callback1.cjs file, and sends a callback for output
    if (err) {
        console.error(err);
    }
    else {
        console.log(data);
    }
}, filePath);